<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class OrderBookForm extends ActiveRecord
{
    //public $verifyCode;

    public static function tableName()
    {
		return '{{order_book}}';
    }
	
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'city', 'number', 'isbn', 'count', 'publish', 'date'], 'required'],
            //['email', 'email'],
            //['verifyCode', 'captcha'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
			'name' => 'ФИО',
			'city' => 'Город',
			'number' => 'Номер',
			'isbn' => 'ISBN',
			'count' => 'Количество',
            //'verifyCode' => 'Verification Code',
        ];
    }

}
