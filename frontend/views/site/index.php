<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\OrderBookForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
//use yii\captcha\Captcha;

$this->title = Yii::$app->name;
?>
<div class="site-index">

    <div class="row">
        <div class="col-lg-8">
            <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'name') ?>

                <?= $form->field($model, 'city') ?>

                <?= $form->field($model, 'number')->textInput(['placeholder'=>'+7 (xxx) xxx-xx-xx'])
                ->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '+9(999)999-99-99',
                    'options' => ['value' => '+7'],
                ]) ?>

                <?= $form->field($model, 'isbn')->label('Выберите книгу')->widget(Select2::classname(), [
                        'language' => \Yii::$app->language,
                        'data' => $data,
                        'options' => ['placeholder' => ''],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                ?>
				
                <?= $form->field($model, 'count')->textInput(['type' => 'number']) ?>
				
                <?= $form->field($model, 'publish')->hiddenInput(['value'=>$publish['id']])->label(false) ?>
				
                <?= $form->field($model, 'date')->hiddenInput(['value'=>time()])->label(false) ?>


				<h3>Издательство для заказа: <a href="<?=$publish['site']?>"><?=$publish['name']?></a> </h3>
				<br>
				
                <div class="form-group">
                    <?= Html::submitButton('Заказать', ['class' => 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
