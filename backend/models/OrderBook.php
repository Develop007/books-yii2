<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "order_book".
 *
 * @property int $id
 * @property string $name
 * @property string $city
 * @property string $number
 * @property string $isbn
 * @property int $count
 * @property int $publish
 * @property int $date
 */
class OrderBook extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_book';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'city', 'number', 'isbn', 'publish', 'date'], 'required'],
            [['count', 'publish', 'date'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['city', 'number', 'isbn'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'city' => 'Город',
            'number' => 'Номер',
            'isbn' => 'ISBN',
            'count' => 'Количество',
            'publish' => 'Издательство',
            'date' => 'Дата',
        ];
    }
}
