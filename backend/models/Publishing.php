<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "publishing".
 *
 * @property int $id
 * @property string $name
 * @property string $formula
 * @property string $site
 */
class Publishing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'publishing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'formula', 'site'], 'required'],
            [['name', 'formula', 'site'], 'string', 'max' => 220],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'formula' => 'Формула',
            'site' => 'Сайт',
        ];
    }
}
