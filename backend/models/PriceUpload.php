<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "price".
 *
 * @property int $id
 */
class PriceUpload extends \yii\db\ActiveRecord
{
    public $csvFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'price';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['isbn'], 'required'],
            [['isbn', 'name', 'authors'], 'string', 'max' => 255],
            [['publish_id', 'year', 'price'], 'integer'],
            [['csvFile'], 'file', 'skipOnEmpty' => false, 'extensions'=>['csv', 'xlsx', 'xls'], 'checkExtensionByMimeType'=>false, 'maxSize'=>1024 * 1024 * 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'isbn' => 'ISBN',
            'name' => 'Наименование',
            'authors' => 'Автор(ы)',
            'publish_id' => 'Издательство',
            'year' => 'Год',
            'price' => 'Цена',
            'csvFile' => 'Файл',
        ];
    }
}
