    <div class="row">
        <div class="col">
			<ul class="nav nav-pills">
                <?= \yii\widgets\Menu::widget([
                    'items' => [
                        ['label' => 'Главная', 'url' => ['site/index']],
                        ['label' => 'Издательства', 'url' => ['publishing/index']],
                        ['label' => 'Заказы', 'url' => ['order-book/index']],
                        ['label' => 'Прайсы', 'url' => ['price/index']],
                        ['label' => 'Загрузка прайсов', 'url' => ['price/upload']],
                    ],
                    'options' => ['class' =>'nav nav-pills']
                ]); ?>
        </div>
    </div>
	<br>