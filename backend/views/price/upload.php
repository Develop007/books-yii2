<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

?>

<?php echo $this->render('@app/views/tabs'); ?>
<div class="instruction" style="padding: 5px 15px; border: 1px solid #ddd;border-radius: 5px;">
  <h4>Порядок столбцов</h4>
  <p>1. Выбираем издательство</p>
  <p>2. Формируем <b>xls / xls / csv</b> в следующем виде:</p>
  <table class="table table-bordered text-center">
    <tr>
      <td>ISBN</td>
      <td>Наименование</td>
      <td>Авторы</td>
      <td>Год</td>
      <td>Цена</td>
    </tr>
  </table>
  <p style="color: #b7b7b7;">4. Рекомендуется в excel установить для ячеек Общий формат, для чисел - Числовой</p>
  <p style="color: #b7b7b7;">5. Расширение файла рекомендуется сохранять в нижнем регистре</p>
</div>
<br><br>
<div class="price-form">
    <?php 
      $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'publish_id')->dropDownList($publish_list) ?>
    <?= 
      $form->field($model, 'csvFile')->widget(FileInput::classname(), [
        'name' => 'input-ru[]',
        'pluginOptions' => [
            'allowedFileExtensions' => ['csv', 'xlsx', 'xls'],
            'showUpload' => false,
          ],
      ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end() ?>
</div>