<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PriceUpload */

$this->title = 'Создание книги';
$this->params['breadcrumbs'][] = ['label' => 'Обновление прайса', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-upload-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
