<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PriceUploadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<?php echo $this->render('@app/views/tabs'); ?>
<div class="price-upload-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить книгу', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Очистить прайс', ['clear'], ['class' => 'btn btn-warning', 'data-confirm' => 'Будут очищены все прайсы!']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'isbn',
            'name',
            'authors',
            'attribute' => 'publish_id',
            'year',
            'price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
