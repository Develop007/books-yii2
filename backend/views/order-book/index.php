<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrderBookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<?php echo $this->render('@app/views/tabs'); ?>
<div class="order-book-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить заказ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'city',
            'number',
            'isbn',
            'count',
            //'publish',
            [
                'attribute' => 'date',
                'format' => ['date', 'php:d/m/Y']

            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
