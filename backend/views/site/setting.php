<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Setting */
/* @var $form ActiveForm */
?>

<?php echo $this->render('@app/views/tabs'); ?>

<div class="instruction">
	<h4>Информация по страницам:</h4>
	<ul>
		<li><i>Главная</i> - издательство для заказа</li>
		<li><i>Издательства</i> - управление издательствами (ссылка на страницу издательства и формула для расчета цены)</li>
		<li><i>Заказы</i> - список оформленных заявок</li>
		<li><i>Прайсы</i> - страница с наименованиями и ценами</li>
		<li><i>Загрузка прайсов</i> - импорт книг по издательству</li>
	</ul>
</div>
<br>
<div class="body-content">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'value')->dropDownList($publish_list) ?>
    
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>
