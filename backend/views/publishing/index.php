<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PublishingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<?php echo $this->render('@app/views/tabs'); ?>
<div class="publishing-index">
    
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создание издательства', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'formula',
            'site',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
