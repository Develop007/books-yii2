<?php

namespace backend\controllers;

use Yii;
use backend\models\PriceUpload;
use backend\models\PriceUploadSearch;
use backend\models\Publishing;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * PriceController implements the CRUD actions for PriceUpload model.
 */
class PriceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'upload', 'clear'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PriceUpload models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PriceUploadSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PriceUpload model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PriceUpload model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PriceUpload();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PriceUpload model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PriceUpload model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionUpload()
    {
        ini_set('max_execution_time', 900);
        $model = new PriceUpload();
        $publish_list = ArrayHelper::map(Publishing::find()->all(), 'id', 'name');

        if ($model->load(Yii::$app->request->post())) {
            $csvFile = UploadedFile::getInstance($model, 'csvFile');

            $explode_name = explode('.', $csvFile->name);
            $ext = end($explode_name);
            if($ext == 'xlsx' || $ext == 'xls')
                $csvFile->tempName = self::excelTransform($csvFile, $ext);

            $publish_id = $model->publish_id;

            $fileName = $csvFile->tempName;
            if (($handle = fopen($fileName, "r")) !== FALSE) {
                while (($data = fgetcsv($handle,0,';')) !== FALSE) {
                    $model = new PriceUpload();
                    for($i = 0; $i <= 4; $i++)
                        if(!isset($data[$i])) $data[$i] = NULL;

                    $model->csvFile=$csvFile;
                    $model->publish_id=$publish_id;

                    $model->isbn=$data[0];
                    $model->name=$data[1];
                    $model->authors=$data[2];
                    $model->year=intval(str_replace(' ', '', $data[3]));
                    $model->price=intval($data[4]);

                    $model->save();
                }
                fclose($handle);
            }
            return $this->redirect(['index']);
        }

        return $this->render('upload', [
            'model' => $model,
            'publish_list' => $publish_list,
        ]);
    }


    public function actionClear()
    {
        $model = new PriceUpload;
        $model::deleteAll();
        return $this->redirect(['index']);
    }

    private function excelTransform($file, $ext)
    {
        // Read the Excel file.
        $reader = IOFactory::createReader(ucfirst(strtolower($ext)));
        $spreadsheet = $reader->load($file->tempName);
         
        // Export to CSV file.
        $writer = IOFactory::createWriter($spreadsheet, "Csv");
        $writer->setSheetIndex(0);
        $writer->setDelimiter(';');
         
        $save_path = tempnam(sys_get_temp_dir(), 'price_csv');
        $writer->save($save_path);

        return $save_path;
    }

    /**
     * Finds the PriceUpload model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PriceUpload the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PriceUpload::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
