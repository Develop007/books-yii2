<?php

use yii\db\Migration;

/**
 * Class m200119_185421_add_column_to_table_user
 */
class m200119_185421_add_column_to_table_user extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'is_admin', $this->integer(1)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'is_admin');
    }
}
