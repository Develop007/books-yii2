<?php

use yii\db\Migration;

/**
 * Class m200119_184655_create_table_setting
 */
class m200119_184655_create_table_setting extends Migration
{
    public function safeUp()
    {
        $this->createTable('setting', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'value' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-publishing_id-value',
            'setting',
            'value',
            'publishing',
            'id',
            'CASCADE'
        );

        $this->insert('setting', [
            'name' => 'active_publish_id',
            'value' => NULL,
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('setting');
    }
}
