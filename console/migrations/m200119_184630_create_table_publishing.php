<?php

use yii\db\Migration;

/**
 * Class m200119_184630_create_table_publishing
 */
class m200119_184630_create_table_publishing extends Migration
{
    public function safeUp()
    {
        $this->createTable('publishing', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'formula' => $this->string(30)->notNull(),
            'site' => $this->string()->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('publishing');
    }
}
