<?php

use yii\db\Migration;

/**
 * Class m191229_210712_create_table_order_book
 */
class m191229_210712_create_table_order_book extends Migration
{
    public function safeUp()
    {
        $this->createTable('order_book', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'city' => $this->string(20)->notNull(),
            'number' => $this->string(20)->notNull(),
            'isbn' => $this->string(20)->notNull(),
            'count' => $this->smallInteger()->notNull()->defaultValue(1),
            'publish' => $this->integer(11)->notNull(),
            'date' => $this->integer()->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('order_book');
    }

}
