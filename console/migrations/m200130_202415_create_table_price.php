<?php

use yii\db\Migration;

/**
 * Class m200130_202415_create_table_price
 */
class m200130_202415_create_table_price extends Migration
{
    public function safeUp()
    {
        $this->createTable('price', [
            'id' => $this->primaryKey(),
            'isbn' => $this->string(),
            'name' => $this->string(),
            'authors' => $this->string(),
            'publish_id' => $this->integer(),
            'year' => $this->integer(),
            'price' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-publishing_id-publish_id',
            'price',
            'publish_id',
            'publishing',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable('price');
    }
}
